#!/bin/bash

## version
VERSION="0.0.1"

## instalation folder
INSTALLATION_FOLDER="/usr/local/bin/"

## Asks for working folder
packagework_askworkingfolder () {
    read -p "Please, provides the project path: " project_path

    if [ ! -d $project_path ] || [ -z $project_path ]; then
        echo You did not provided a valid path: $project_path. Does it exists and is a directory?
        packagework_askworkingfolder
        return 1
    fi

    return 0
}

## main function
packagework () {
    packagework_askworkingfolder
}


## detect if being sourced and
## export if so else execute
## main function with args
if [[ ${BASH_SOURCE[0]} != $0 ]]; then
  export -f packagework
else
  packagework "${@}"
  exit $?
fi

