
BIN ?= packagework
PREFIX ?= /usr/local

install:
	cp packagework.sh $(PREFIX)/bin/$(BIN)
	chmod +x $(PREFIX)/bin/$(BIN)

uninstall:
	rm -f $(PREFIX)/bin/$(BIN)
